// 


/*


*/

// [SECTION] Statements and Syntax
	// Statements
		// In programming language, statements are instruction that we tell the computerto perform.
		// JS statements usually ends with semicolon (;)
			// (;) is also called as delimeter.
		// Semicolons are not required in JS.
			// it is used us to train or locate where a statement ends.
	// Syntax
		// In programming,it is the set of rules that describes how statements must be constructed.

console.log("Hello World");

// [SECTION] Variables

// It is used to contain data.


let myVariable;
console.log(myVariable);

// console.log(Hello);

myVariable = "hello";

myVariable = "Hello World";
console.log(myVariable);


	// const PI;

	// PI = 3.1416;
	// console.log(PI); //error: Initial valueismissingfor const.

// Declaring with initialization
	// a variable is given it's initial/starting value upon declaration.
		// Syntax: let/const variableName = value;

		let productName = "Desktop Computer"
		console.log(productName);

		let productPrice = 18999;
		productPrice = 20000;
		console.log(productPrice);

		const PI = 3.1416;
		// PI = 3.14; error: assignment to constant variable.
		console.log(PI);

/*
	Guide in writing variables.
	1. Use the "let" keyword if the variable will contain different values/can change over time.
		Naming convention: Variable name should starts with "lower characters" characters and "camelCasing" is use for multiple words.
	
	2. use the "const"keyword if the variable does not changed.
		Naming Convention: "const" keyword should be followed by ALL CAPITAL VARIABLE NAME (single value variable)
	
	3. variable name should be indicative(or descriptive) of the value being tored to avoid confusion

	Avoid this one:let word = "John Smith"

*/
// MUltiple variable declaration
		let productCode = "DC017", productBrand = "Dell";
		console.log(productCode, productBrand);

		// const let = "hello";
		// console.log(let); error: lexically body is disallowed.

// [SECTION] Data Types

// In JavaScript, there are six types of data

	//String
		// are series of characters that create a word, a phrase, a sentence or anything related to creating text.
		// Strings in JavaScript can be written using a single quote('') or double quote("")

		let country = "Philippines";
		let province = 'Metro Manila';

		//  Concatenating Strings
		// Multiple string values that can be combined to create a single String using the "+" symbol.
		// I live in Metro Manila, Philippines
		let greeting = "I live in " + province + ", " + country;
		console.log(greeting);

		//  Escape Characters
		let mailAddress =  "Quezon City\nPhilippines";
		console.log(mailAddress);

		//Expected output: John's employees home early
		let message = "John's employees went home early";
		console.log(message);

		// using the escape characters (\)
		message = 'Jane\'s employees went home early';
		console.log(message);

	// Numbers
		// includespositive,negative
		// Integers/whole Numbers
		let headCount = 26;
		console.log(headCount);

		// 
		let grade = 98.7;
		console.log(grade);

		// Exponential 
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combine number and strings
		console.log("John's grade last quarter is " + grade);

	// Boolean
		// Boolean values are normally used to store values relating to the state of certain things.
		// true or false (two logical values)

		let isMarried = false;
		let isGoodConduct = true;

		console.log("isMarried: "+isMarried);
		console.log("isGoodConduc: "+isGoodConduct);


	// Arrays
		// are special kind of data type that can usedto store multiple related values
		// Syntax: let/const arrayName = [elementA, elementB, ... elementNth];

		const grades = [98.7, 92.1, 90.2, 94.6];
		// constant variable cannot be reassigned.
		//grades = 100; not allowed
		//Changing the elements of an array or changing the properties of an object is allowed in constant variable.
		grades[0] = 100; //reference value
		console.log(grades);

		let details = ["John", "Smith", 32, true];
		console.log(details);

	//Objects
		// are another special kind of data type that is used to mimic real world objects/items.
		// used to create complex data that contains information relevant to each other.
		/*
			Syntax:
				let/cont objectName = {
					propertyA: valueA,
					propertyB: valueB
				}
		*/
		let person ={
			fullName: "Juan Dela Cruz",
			age: 35,
			isMarried:false,
			contact: ["+63912 345 6789", "8123 456"],
			address: {
				houseNumber: "345",
				city: "Manila"
			}
		}

		console.log(person);

		// 

		let myGrades ={
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6
		}

		console.log(myGrades);

		// Typeof operator is used to determined the type of data or the value of a variable.

		console.log(typeof myGrades); //object

		// arrays isa special type of object with methods.
		console.log(typeof grades); //object

	// Null
		// indicates the absence of a value.
		let spouse = null;
		console.log(spouse);

	// Undefined
		// indicates that a variable has not been given a value yet.
		let fullName;
		console.log(fullName);


